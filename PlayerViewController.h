//
//  PlayerViewController.h
//  Lorrha
//
//  Created by Kenny Sanchez on 3/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "AppManager.h"
#import "Asset.h"

#import "PlayerView.h"
@class AVPlayer;

@interface PlayerViewController : UIViewController 

@property (nonatomic, strong) NSTimer  *hideToolBarsTimer;

@property (nonatomic, strong)IBOutlet PlayerView *playerView;

@property (nonatomic, strong) AVPlayer *player;
@property (nonatomic, strong) AVPlayerItem *playerItem;

@property (nonatomic, strong) NSBundle *bundle;
@property (nonatomic, strong) AVMutableComposition *languageVidoeComposition;

@property (nonatomic, strong) IBOutlet UIToolbar *toolbar;
@property (nonatomic, strong) IBOutlet UISlider *scrubber;

@property (nonatomic, strong) IBOutlet UIBarButtonItem *playBarButtonItem;
@property (nonatomic, strong) IBOutlet UIBarButtonItem *pauseBarButonItem;

@property (nonatomic, assign) float restoreAfterScrubbingRate;
@property (nonatomic, assign) BOOL  seekToZeroBeforePlay;
@property (nonatomic, strong) id    timeObserver;

@property (nonatomic, strong) Asset *videoAsset;


- (void)loadLanguageVideoComposition;
- (IBAction)loadAssetFromFile:sender;

- (IBAction)play:(id)sender;
- (IBAction)pause:(id)sender;

- (void)showPlayButton;
- (void)showStopButton;

- (void)syncScrubber;
- (void)syncPlayPauseButtons;

- (void)assetFailedToPrepareForPlayback:(NSError *)error;
- (BOOL)isPlaying;

- (void)prepareToPlayAsset:(AVURLAsset *)asset withKeys:(NSArray *)requestedKeys;
- (IBAction)hide:(id)sender; 


@end
