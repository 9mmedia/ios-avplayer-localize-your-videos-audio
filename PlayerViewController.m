//
//  PlayerViewController.m
//  Lorrha
//
//  Created by Kenny Sanchez on 3/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PlayerViewController.h"

/* Asset keys */
NSString * const kTracksKey       = @"tracks";
NSString * const kPlayableKey		  = @"playable";

/* PlayerItem keys */
NSString * const kStatusKey       = @"status";

/* AVPlayer keys */
NSString * const kRateKey			    = @"rate";
NSString * const kCurrentItemKey	= @"currentItem";


static void *AVPlayerViewControllerRateObservationContext = &AVPlayerViewControllerRateObservationContext;
static void *AVPlayerViewControllerStatusObservationContext = &AVPlayerViewControllerStatusObservationContext;
static void *AVPlayerViewControllerCurrentItemObservationContext = &AVPlayerViewControllerCurrentItemObservationContext;

//Implementation
@implementation PlayerViewController 

@synthesize hideToolBarsTimer = _hideToolBarsTimer;
@synthesize videoAsset = _videoAsset;
@synthesize playerView = _playerView;

@synthesize player = _player;
@synthesize playerItem = _playerItem;

@synthesize bundle = _bundle;
@synthesize languageVidoeComposition = _languageVidoeComposition;

@synthesize toolbar = _toolbar;
@synthesize scrubber = _scrubber;

@synthesize playBarButtonItem = _playBarButtonItem;
@synthesize pauseBarButonItem = _pauseBarButonItem;

@synthesize restoreAfterScrubbingRate = _restoreAfterScrubbingRate;
@synthesize seekToZeroBeforePlay = _seekToZeroBeforePlay;

@synthesize timeObserver = _timeObserver;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void) dealloc
{
  [self removePlayerTimeObserver];
  
  [[self player] removeObserver:self forKeyPath:kRateKey];
  [[self player] removeObserver:self forKeyPath:kCurrentItemKey];
  [[self player].currentItem removeObserver:self forKeyPath:kStatusKey];
  [[self player] pause];
  
  [self setHideToolBarsTimer:nil];
  [self setPlayerView:nil];
  [self setPlayer:nil];
  [self setPlayerItem:nil];
  [self setBundle:nil];
  [self setLanguageVidoeComposition:nil];
  [self setToolbar:nil];
  [self setScrubber:nil];
  [self setPlayBarButtonItem:nil];
  [self setPauseBarButonItem:nil];
  [self setVideoAsset:nil];
}


#pragma mark - AVPlayerViewController 

- (void)loadLanguageVideoComposition 
{
  NSString *path = [[ NSBundle mainBundle ] pathForResource:[[AppManager sharedAppManager] appLanguage] ofType:@"lproj"];
  NSBundle *bundle = [NSBundle bundleWithPath:path];
  //AUDIO ASSET
  NSURL *audioURL = [bundle
                     URLForResource:[_videoAsset assetID] withExtension:@"mp3"];
  AVURLAsset *audioURLAsset = [AVURLAsset URLAssetWithURL:audioURL options:nil];
  
  AVMutableCompositionTrack *compositionAudioTrack = [_languageVidoeComposition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
  AVAssetTrack *clipAudioTrack = [[audioURLAsset tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0];
  [compositionAudioTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, [audioURLAsset duration])  ofTrack:clipAudioTrack atTime:kCMTimeZero error:nil];
}

- (IBAction)loadAssetFromFile:sender 
{
  NSURL *fileURL = [[NSBundle mainBundle]
                    URLForResource:[_videoAsset assetID] withExtension:@"mp4"];
  AVURLAsset *asset = [AVURLAsset URLAssetWithURL:fileURL options:nil];
  
  [asset loadValuesAsynchronouslyForKeys:[NSArray arrayWithObject:kTracksKey] completionHandler:
   ^{
     // create an instance of AVPlayerItem for the asset, and set it as the player for the player view.
     dispatch_async(dispatch_get_main_queue(),
                    ^{
                      //Add Video Track
                      _languageVidoeComposition = [AVMutableComposition composition];
                      AVMutableCompositionTrack *compositionVideoTrack = [_languageVidoeComposition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
                      AVAssetTrack *clipVideoTrack = [[asset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
                      [compositionVideoTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, [asset duration])  ofTrack:clipVideoTrack atTime:kCMTimeZero error:nil];
                      
                      [self loadLanguageVideoComposition];
                      
                      NSError *error = nil;
                      AVKeyValueStatus status = [asset statusOfValueForKey:kTracksKey error:&error];
                      
                      if ((status == AVKeyValueStatusLoaded) && _languageVidoeComposition) {
                        
                        [self setPlayerItem:[AVPlayerItem playerItemWithAsset:_languageVidoeComposition]];
                        
                        [_playerItem addObserver:self forKeyPath:kStatusKey
                                         options:0 context:AVPlayerViewControllerStatusObservationContext];
                        [[NSNotificationCenter defaultCenter] addObserver:self
                                                                 selector:@selector(playerItemDidReachEnd:)
                                                                     name:AVPlayerItemDidPlayToEndTimeNotification
                                                                   object:_playerItem];
                        
                        [self setPlayer:[AVPlayer playerWithPlayerItem:_playerItem]];
                        
                        
                        [_playerView setPlayer:_player];
                        
                        // Register with the notification center after creating the player item.
                        [[NSNotificationCenter defaultCenter]
                         addObserver:self
                         selector:@selector(playerItemDidReachEnd:)
                         name:AVPlayerItemDidPlayToEndTimeNotification
                         object:[_player currentItem]];
                        
                        NSArray *requestedKeys = [NSArray arrayWithObjects:kTracksKey, kPlayableKey, nil];
                        [self prepareToPlayAsset:asset withKeys:requestedKeys];
                      }
                      else {
                        // You should deal with the error appropriately.
                        NSLog(@"The asset's tracks were not loaded:\n%@", [error localizedDescription]);
                      }
                    });
   }];
  
}

#pragma mark Prepare to play asset, URL

/*
 Invoked at the completion of the loading of the values for all keys on the asset that we require.
 Checks whether loading was successfull and whether the asset is playable.
 If so, sets up an AVPlayerItem and an AVPlayer to play the asset.
 */
- (void)prepareToPlayAsset:(AVURLAsset *)asset withKeys:(NSArray *)requestedKeys
{
  /* Make sure that the value of each key has loaded successfully. */
	for (NSString *thisKey in requestedKeys)
	{
		NSError *error = nil;
		AVKeyValueStatus keyStatus = [asset statusOfValueForKey:thisKey error:&error];
		if (keyStatus == AVKeyValueStatusFailed)
		{
			[self assetFailedToPrepareForPlayback:error];
			return;
		}
	}
  
  /* Use the AVAsset playable property to detect whether the asset can be played. */
  if (!asset.playable) 
  {
		NSString *localizedDescription = NSLocalizedString(@"Item cannot be played", @"Item cannot be played description");
		NSString *localizedFailureReason = NSLocalizedString(@"The assets tracks were loaded, but could not be made playable.", @"Item cannot be played failure reason");
		NSDictionary *errorDict = [NSDictionary dictionaryWithObjectsAndKeys:
                               localizedDescription, NSLocalizedDescriptionKey, 
                               localizedFailureReason, NSLocalizedFailureReasonErrorKey, 
                               nil];
		NSError *assetCannotBePlayedError = [NSError errorWithDomain:@"StitchedStreamPlayer" code:0 userInfo:errorDict];
    
    /* Display the error to the user. */
    [self assetFailedToPrepareForPlayback:assetCannotBePlayedError];
    
    return;
  }
	
  
  /* Stop observing our prior AVPlayerItem, if we have one. */
  if ([self playerItem])
  {
    /* Remove existing player item key value observers and notifications. */
    
    [[self playerItem] removeObserver:self forKeyPath:kStatusKey];            
		
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:AVPlayerItemDidPlayToEndTimeNotification
                                                  object:[self playerItem]];
  }
	
  /* Observe the player item "status" key to determine when it is ready to play. */
  [[self playerItem] addObserver:self 
                     forKeyPath:kStatusKey 
                        options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
                        context:AVPlayerViewControllerStatusObservationContext];
	
  /* When the player item has played to its end time we'll toggle
   the movie controller Pause button to be the Play button */
  [[NSNotificationCenter defaultCenter] addObserver:self
                                           selector:@selector(playerItemDidReachEnd:)
                                               name:AVPlayerItemDidPlayToEndTimeNotification
                                             object:[self playerItem]];
	
  _seekToZeroBeforePlay = NO;
  
  /* Observe the AVPlayer "currentItem" property to find out when any 
   AVPlayer replaceCurrentItemWithPlayerItem: replacement will/did 
   occur.*/
  [[self player] addObserver:self 
                  forKeyPath:kCurrentItemKey 
                     options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
                     context:AVPlayerViewControllerCurrentItemObservationContext];
  
  /* Observe the AVPlayer "rate" property to update the scrubber control. */
  [[self player] addObserver:self 
                  forKeyPath:kRateKey 
                     options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew
                     context:AVPlayerViewControllerRateObservationContext];
	
  /* Make our new AVPlayerItem the AVPlayer's current item. */
  if (self.player.currentItem != [self playerItem])
  {
    /* Replace the player item with a new player item. The item replacement occurs 
     asynchronously; observe the currentItem property to find out when the 
     replacement will/did occur*/
    [[self player] replaceCurrentItemWithPlayerItem:[self playerItem]];
    
    [self syncPlayPauseButtons];
  }
	
  [_scrubber setValue:0.0];
}

#pragma mark -
#pragma mark Movie controller methods

#pragma mark
#pragma mark Button Action Methods

- (IBAction)play:(id)sender
{
	/* If we are at the end of the movie, we must seek to the beginning first 
   before starting playback. */
	if (YES == _seekToZeroBeforePlay) 
	{
		_seekToZeroBeforePlay = NO;
		[_player seekToTime:kCMTimeZero];
	}
  
	[_player play];
	
  [self showStopButton];    
  
  [self hide:sender];
}

- (IBAction)pause:(id)sender
{
	[_player pause];
  
  [self showPlayButton];
  [self hide:sender];
}

#pragma mark -
#pragma mark Play, Stop buttons

/* Show the stop button in the movie player controller. */
-(void)showStopButton
{
  NSMutableArray *toolbarItems = [NSMutableArray arrayWithArray:[self toolbarItems]];
  [toolbarItems replaceObjectAtIndex:0 withObject:_pauseBarButonItem];
  [self setToolbarItems:toolbarItems];
}

/* Show the play button in the movie player controller. */
-(void)showPlayButton
{
  NSMutableArray *toolbarItems = [NSMutableArray arrayWithArray:[self toolbarItems]];
  [toolbarItems replaceObjectAtIndex:0 withObject:_playBarButtonItem];
  [self setToolbarItems:toolbarItems];
}

/* If the media is playing, show the stop button; otherwise, show the play button. */
- (void)syncPlayPauseButtons
{
	if ([self isPlaying])
	{
    [self showStopButton];
	}
	else
	{
    [self showPlayButton];        
	}
}

-(void)enablePlayerButtons
{
  [[self playBarButtonItem] setEnabled:YES];
  [[self pauseBarButonItem] setEnabled:YES];
}

-(void)disablePlayerButtons
{
  [[self playBarButtonItem] setEnabled:NO];
  [[self pauseBarButonItem] setEnabled:NO];
}

#pragma mark Player Item

- (BOOL)isPlaying
{
	return _restoreAfterScrubbingRate != 0.f || [_player rate] != 0.f;
}

/* Called when the player item has played to its end time. */
- (void)playerItemDidReachEnd:(NSNotification *)notification 
{
	/* After the movie has played to its end time, seek back to time zero 
   to play it again. */
	_seekToZeroBeforePlay = YES;
}

/* ---------------------------------------------------------
 **  Get the duration for a AVPlayerItem. 
 ** ------------------------------------------------------- */

- (CMTime)playerItemDuration
{
	AVPlayerItem *playerItem = [_player currentItem];
	if (playerItem.status == AVPlayerItemStatusReadyToPlay)
	{
		return([playerItem duration]);
	}
	
	return(kCMTimeInvalid);
}


/* Cancels the previously registered time observer. */
-(void)removePlayerTimeObserver
{
	if (_timeObserver)
	{
		[_player removeTimeObserver:_timeObserver];
		_timeObserver = nil;
	}
}



#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];

    //Rotate to landscape
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    UIView *view = [window.subviews objectAtIndex:0];
    [view removeFromSuperview];
    [window addSubview:view];  

    [[[self navigationController] navigationBar] setBarStyle:UIBarStyleBlackTranslucent];
    
    [[self navigationController] setToolbarHidden:NO];
    [[[self navigationController] toolbar] setBarStyle:UIBarStyleBlackTranslucent];
  
    UIBarButtonItem *scrubberItem = [[UIBarButtonItem alloc] initWithCustomView:_scrubber];
    UIBarButtonItem *flexItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    [self setToolbarItems:[NSArray arrayWithObjects:_playBarButtonItem, flexItem, scrubberItem, flexItem, nil]];
  
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    [[AVAudioSession sharedInstance] setActive: YES error: nil];
  
    [self loadAssetFromFile:nil];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    [self removePlayerTimeObserver];
    [self setHideToolBarsTimer:nil];
    [self setPlayerView:nil];
    [self setPlayer:nil];
    [self setPlayerItem:nil]; 
    [self setBundle:nil];
    [self setLanguageVidoeComposition:nil];
    [self setToolbar:nil];
    [self setScrubber:nil];
    [self setPlayBarButtonItem:nil];
    [self setPauseBarButonItem:nil];
    [self setVideoAsset:nil];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return ((interfaceOrientation == UIInterfaceOrientationLandscapeRight) || (interfaceOrientation == UIInterfaceOrientationLandscapeLeft) || UIInterfaceOrientationIsLandscape(interfaceOrientation));

}

- (void)viewWillAppear:(BOOL)animated {
  //Rotate to landscape
}

- (void)viewWillDisappear:(BOOL)animated
{
  [super viewWillDisappear:animated];
  [self removePlayerTimeObserver];
  [[self player] removeObserver:self forKeyPath:kRateKey];
  [[self player] removeObserver:self forKeyPath:kCurrentItemKey];
  [[self player].currentItem removeObserver:self forKeyPath:kStatusKey];
  [[self player] pause];
  [self setPlayer:nil];
  
  [[self navigationController] setToolbarHidden:YES];
}


-(void)setViewDisplayName
{
  /* Set the view title to the last component of the asset URL. */
//  self.title = [_URL lastPathComponent];
  
  /* Or if the item has a AVMetadataCommonKeyTitle metadata, use that instead. */
	for (AVMetadataItem* item in ([[[_player currentItem] asset] commonMetadata]))
	{
		NSString* commonKey = [item commonKey];
		
		if ([commonKey isEqualToString:AVMetadataCommonKeyTitle])
		{
			self.title = [item stringValue];
		}
	}
}


#pragma mark -
#pragma mark Movie scrubber control

/* ---------------------------------------------------------
 **  Methods to handle manipulation of the movie scrubber control
 ** ------------------------------------------------------- */

/* Requests invocation of a given block during media playback to update the movie scrubber control. */
-(void)initScrubberTimer
{
	double interval = .1f;	
	
	CMTime playerDuration = [self playerItemDuration];
	if (CMTIME_IS_INVALID(playerDuration)) 
	{
		return;
	} 
	double duration = CMTimeGetSeconds(playerDuration);
	if (isfinite(duration))
	{
		CGFloat width = CGRectGetWidth([_scrubber bounds]);
		interval = 0.5f * duration / width;
	}
  
	/* Update the scrubber during normal playback. */
  __weak PlayerViewController *avPlayerViewController = self; 
	_timeObserver = [_player addPeriodicTimeObserverForInterval:CMTimeMakeWithSeconds(interval, NSEC_PER_SEC) 
                                                         queue:NULL /* If you pass NULL, the main queue is used. */
                                                    usingBlock:^(CMTime time) 
                    {
                      [avPlayerViewController syncScrubber];
                    }];
  
}

/* Set the scrubber based on the player current time. */
- (void)syncScrubber
{
	CMTime playerDuration = [self playerItemDuration];
	if (CMTIME_IS_INVALID(playerDuration)) 
	{
    [_scrubber setMinimumValue:0.0];
		return;
	} 
  
	double duration = CMTimeGetSeconds(playerDuration);
	if (isfinite(duration))
	{
		float minValue = [_scrubber minimumValue];
		float maxValue = [_scrubber maximumValue];
		double time = CMTimeGetSeconds([_player currentTime]);
		
		[_scrubber setValue:(maxValue - minValue) * time / duration + minValue];
	}
}

/* The user is dragging the movie controller thumb to scrub through the movie. */
- (IBAction)beginScrubbing:(id)sender
{
	_restoreAfterScrubbingRate = [_player rate];
	[_player setRate:0.f];
	
	/* Remove previous timer. */
	[self removePlayerTimeObserver];
}

/* Set the player current time to match the scrubber position. */
- (IBAction)scrub:(id)sender
{
	if ([sender isKindOfClass:[UISlider class]])
	{
		UISlider* slider = sender;
		
		CMTime playerDuration = [self playerItemDuration];
		if (CMTIME_IS_INVALID(playerDuration)) {
			return;
		} 
		
		double duration = CMTimeGetSeconds(playerDuration);
		if (isfinite(duration))
		{
			float minValue = [slider minimumValue];
			float maxValue = [slider maximumValue];
			float value = [slider value];
			
			double time = duration * (value - minValue) / (maxValue - minValue);
			
			[_player seekToTime:CMTimeMakeWithSeconds(time, NSEC_PER_SEC)];
		}
	}
}

/* The user has released the movie thumb control to stop scrubbing through the movie. */
- (IBAction)endScrubbing:(id)sender
{
	if (!_timeObserver)
	{
		CMTime playerDuration = [self playerItemDuration];
		if (CMTIME_IS_INVALID(playerDuration)) 
		{
			return;
		} 
		
		double duration = CMTimeGetSeconds(playerDuration);
		if (isfinite(duration))
		{
			CGFloat width = CGRectGetWidth([_scrubber bounds]);
			double tolerance = 0.5f * duration / width;
      
      __weak PlayerViewController *avPlayerViewController = self; 
			_timeObserver = [_player addPeriodicTimeObserverForInterval:CMTimeMakeWithSeconds(tolerance, NSEC_PER_SEC) queue:NULL usingBlock:
                        ^(CMTime time)
                        {
                          [avPlayerViewController syncScrubber];
                        }];
		}
	}
  
	if (_restoreAfterScrubbingRate)
	{
		[_player setRate:_restoreAfterScrubbingRate];
		_restoreAfterScrubbingRate = 0.f;
	}
}

- (BOOL)isScrubbing
{
	return _restoreAfterScrubbingRate != 0.f;
}

-(void)enableScrubber
{
  [[self scrubber] setEnabled:YES];
}

-(void)disableScrubber
{
  [[self scrubber] setEnabled:NO];
}

#pragma mark -
#pragma mark Asset Key Value Observing
#pragma mark

#pragma mark Key Value Observer for player rate, currentItem, player item status

- (void)observeValueForKeyPath:(NSString*) path 
                      ofObject:(id)object 
                        change:(NSDictionary*)change 
                       context:(void*)context
{
	/* AVPlayerItem "status" property value observer. */
	if (context == AVPlayerViewControllerStatusObservationContext)
	{
		[self syncPlayPauseButtons];
    
    AVPlayerStatus status = [[change objectForKey:NSKeyValueChangeNewKey] integerValue];
    switch (status)
    {
        /* Indicates that the status of the player is not yet known because 
         it has not tried to load new media resources for playback */
      case AVPlayerStatusUnknown:
      {
        [self removePlayerTimeObserver];
        [self syncScrubber];
        
        [self disableScrubber];
        [self disablePlayerButtons];
      }
        break;
        
      case AVPlayerStatusReadyToPlay:
      {
        /* Once the AVPlayerItem becomes ready to play, i.e. 
         [playerItem status] == AVPlayerItemStatusReadyToPlay,
         its duration can be fetched from the item. */
        
        [self initScrubberTimer];
        
        [self enableScrubber];
        [self enablePlayerButtons];
      }
        break;
        
      case AVPlayerStatusFailed:
      {
        AVPlayerItem *playerItem = (AVPlayerItem *)object;
        [self assetFailedToPrepareForPlayback:playerItem.error];
      }
        break;
    }
	}
	/* AVPlayer "rate" property value observer. */
	else if (context == AVPlayerViewControllerRateObservationContext)
	{
    [self syncPlayPauseButtons];
	}
	/* AVPlayer "currentItem" property observer. 
   Called when the AVPlayer replaceCurrentItemWithPlayerItem: 
   replacement will/did occur. */
	else if (context == AVPlayerViewControllerCurrentItemObservationContext)
	{
    AVPlayerItem *newPlayerItem = [change objectForKey:NSKeyValueChangeNewKey];
    
    /* Is the new player item null? */
    if (newPlayerItem == (id)[NSNull null])
    {
      [self disablePlayerButtons];
      [self disableScrubber];
    }
    else /* Replacement of player currentItem has occurred */
    {
      /* Set the AVPlayer for which the player layer displays visual output. */
      [_playerView setPlayer:_player];
      
      [self setViewDisplayName];
      
      /* Specifies that the player should preserve the video’s aspect ratio and 
       fit the video within the layer’s bounds. */
      [_playerView setVideoFillMode:AVLayerVideoGravityResize];
      
      [self syncPlayPauseButtons];
    }
	}
	else
	{
		[super observeValueForKeyPath:path ofObject:object change:change context:context];
	}
}

#pragma mark -
#pragma mark Error Handling - Preparing Assets for Playback Failed

-(void)assetFailedToPrepareForPlayback:(NSError *)error
{
  [self removePlayerTimeObserver];
  [self syncScrubber];
  [self disableScrubber];
  [self disablePlayerButtons];
  
  /* Display the error. */
	UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[error localizedDescription]
                                                      message:[error localizedFailureReason]
                                                     delegate:nil
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil];
	[alertView show];
}


#pragma mark Hide Top and Bottom Bars

-(IBAction)hide:(id)sender 
{
  
  if ([[[self  navigationController] navigationBar] isHidden]) {
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    [[self navigationController] setToolbarHidden:NO animated:YES];
    
    _hideToolBarsTimer = [NSTimer scheduledTimerWithTimeInterval:2.0
                                                          target:self
                                                        selector:@selector(cancelTimer:)
                                                        userInfo:nil
                                                         repeats:NO];
  }
  else {
    if (_hideToolBarsTimer) {
      [_hideToolBarsTimer invalidate];
    }
    _hideToolBarsTimer = nil;
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [[self navigationController] setToolbarHidden:YES animated:YES];
  }
}

- (void)cancelTimer:(NSTimer *)nsTimer
{
  [self hide:nil];
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event 
{
  [super touchesBegan:touches withEvent:event];
  [self hide:nil];
}

- (void)navigationBarDoubleTap:(UIGestureRecognizer*)recognizer {
  if (_hideToolBarsTimer) {
    [_hideToolBarsTimer invalidate];
  }
  _hideToolBarsTimer = nil;
  
  
  _hideToolBarsTimer = [NSTimer scheduledTimerWithTimeInterval:5.0
                                                        target:self
                                                      selector:@selector(cancelTimer:)
                                                      userInfo:nil
                                                       repeats:NO];
}


@end
